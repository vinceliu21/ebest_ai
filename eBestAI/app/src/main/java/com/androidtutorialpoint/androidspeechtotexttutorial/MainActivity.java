package com.androidtutorialpoint.androidspeechtotexttutorial;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Locale;


import edu.stanford.nlp.dcoref.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;

import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

//import org.tensorflow.lite.Interpreter;
import java.util.*;

public class MainActivity extends AppCompatActivity {

    private HashMap<String, String> map = new HashMap<String, String>();


    private RadioGroup languageGroup;
    private RadioButton languageButton;
    private Button langDisplay;
    private Button orderDisplay;

    private TextView coke_bottles;
    private TextView sprite_bottles;
    private TextView voiceInput;
    private TextView speakButton;
    private TextToSpeech textToSpeech;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private int coke_order = 0;
    private int sprite_order = 0;
    private int lang = 0;

    public StanfordCoreNLP pipeline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coke_bottles = (TextView) findViewById(R.id.coke_bottles);
        sprite_bottles = (TextView) findViewById(R.id.sprite_bottles);


        map.put("VBP", "verb");
        map.put("VB", "verb");
        map.put("DT", "determiner");
        map.put("NN", "noun");
        map.put("CC", "conjunction");
        map.put("CD", "cardinal number");
        map.put("JJ", "adjective");
        map.put("UH", "interjection");
        map.put("RP", "particle");
        map.put("RB", "adverb");
        map.put("NNS", "plural noun");
        map.put("EX", "existential there");
        map.put("FW", "foreign word");
        map.put("IN", "preposition");
        map.put("JJR", "comparative adjective");
        map.put("JJS", "superlative adjective");
        map.put("LS", "list item marker");
        map.put("MD", "modal");
        map.put("NNP", "proper noun singular");
        map.put("NNPS", "proper noun plural");
        map.put("PDT", "predeterminer");
        map.put("POS", "possessive ending");
        map.put("PRP", "personal pronoun");
        map.put("PRPS", "possessive pronoun");
        map.put("RBR", "comparative adverb");
        map.put("RBS", "superlative adverb");

        map.put("SYM", "symbol");
        map.put("TO", "to");
        map.put("VBD", "past tense verb");
        map.put("VBG", "gerund verb");
        map.put("VBN", "past participle verb");
        map.put("VBZ", "third person verb");
        map.put("WDT", "wh determiner");
        map.put("WP", "wh pronoun");
        map.put("WPS", "posessive wh pronoun");
        map.put("WRB", "wh adverb");



        //Stanford CoreNLP AI initializer (START INITIALIZATION CODE CHUNK)
        Properties props = new Properties();
        //You can set certain properties here
        props.setProperty("annotators", "tokenize, ssplit, pos, lemma");

        Log.d("Debugger", "before pipeline");
        pipeline = new StanfordCoreNLP(props);
        Log.d("Debugger", "after pipeline");

        //END OF INITIALIZATION CODE



        voiceInput = (TextView) findViewById(R.id.voiceInput);
        speakButton = (TextView) findViewById(R.id.btnSpeak);

        speakButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                askSpeechInput();
            }
        });

        addButton();

        orderDisplay = (Button) findViewById(R.id.orderDisplay);

        orderDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(lang){
                    case 0:
                        textToSpeech.speak("Your next store location is Shanghai Store Number One. The current order status to refill is five coke bottles and three sprite bottles. Thank you.", TextToSpeech.QUEUE_FLUSH, null);
                        break;
                    case 1:
                        textToSpeech.speak("您的下一个商店位置是上海第一商店。目前订购的订单状态是五个可乐瓶和三个精灵瓶。谢谢", TextToSpeech.QUEUE_FLUSH, null);
                        break;
                    case 2:
                        textToSpeech.speak("次の店舗は上海店のナンバーワンです。現在補充する注文状況は、5本のコークスボトルと3本のスプライトボトルです。ありがとうございました", TextToSpeech.QUEUE_FLUSH, null);
                        break;

                }
            }
        });


        textToSpeech = new TextToSpeech(this, new MyTTSInitListener());
    }

    public void addButton(){

        languageGroup = (RadioGroup) findViewById(R.id.langGroup);
        langDisplay = (Button) findViewById(R.id.langDisplay);
        langDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedID = languageGroup.getCheckedRadioButtonId();

                languageButton = (RadioButton) findViewById(selectedID);
                CharSequence t = languageButton.getText();


                if (t.toString().equals("English")){
                    Log.i("TTT", "Break");
                    int result = textToSpeech.setLanguage(Locale.US);
                    textToSpeech.speak("Hello Sales Rep", TextToSpeech.QUEUE_FLUSH, null);
                    lang = 0;

                } else if (t.toString().equals("中文")){
                    Log.i("TTT", "break1");
                    int result = textToSpeech.setLanguage(Locale.CHINA);
                    textToSpeech.speak("你好销售代表", TextToSpeech.QUEUE_FLUSH, null);
                    lang = 1;
                } else if (t.toString().equals("Japan")){
                    int result = textToSpeech.setLanguage(Locale.JAPAN);
                    textToSpeech.speak("こんにちは営業担当者", TextToSpeech.QUEUE_FLUSH, null);
                    lang = 2;
                }

            }
        });


    }


    // Showing google speech input dialog
    private void askSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "")
        //intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        //intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello Sales Rep, speak your request here");
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {

        }
    }

    // Receiving speech input
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //voiceInput.setText(result.get(0));



                    // NLP/ML and AI here (START OF NLP AI CODE)
                    //String messageToSpeak = (String) voiceInput.getText();

                    String messageToSpeak = result.get(0);
                    Log.i("debugger", messageToSpeak);

                    String[] words = messageToSpeak.split("\\s+");
                    for (int i = 0; i < words.length; i++) {
                        // You may want to check for a non-word character before blindly
                        // performing a replacement
                        // It may also be necessary to adjust the character class
                        words[i] = words[i].replaceAll("[^\\w]", "");
                        Log.i("debugger", words[i]);
                    }


                    String init_word = words[0];
                    Log.i("debugger", init_word);

                    if (init_word.equals("order"))
                    {
                        Log.i("debugger", "inside order");
                        int order = Integer.valueOf(words[1]);
                        String type = words[2];
                        if (type.equals("coke")){
                            coke_order = coke_order + order;
                            coke_bottles.setText("Coke Bottles: " + coke_order);
                            textToSpeech.speak("Order placed. Thank you.", TextToSpeech.QUEUE_FLUSH, null);
                        } else if (type.equals("sprite"))
                        ;;{
                            sprite_order = sprite_order + order;
                            sprite_bottles.setText("Sprite Bottles: " + sprite_order);
                            textToSpeech.speak("Order placed. Thank you.", TextToSpeech.QUEUE_FLUSH, null);
                        }
                        Log.i("debugger", "order");
                    }else if (init_word.equals("coke")) {

                        //textToSpeech.speak(ft, TextToSpeech.QUEUE_FLUSH, null);
                        textToSpeech.speak(String.format("You have %s coke bottles ordered.", String.valueOf(coke_order)), TextToSpeech.QUEUE_FLUSH, null);


                    } else if (init_word.equals("sprite")) {
                        textToSpeech.speak(String.format("You have %s sprite bottles ordered.", String.valueOf(sprite_order)), TextToSpeech.QUEUE_FLUSH, null);
                    }

//                    Annotation document = new Annotation(messageToSpeak);
//                    pipeline.annotate(document);
//
//                    List<String> words = new ArrayList<>();
//                    List<String> POS_list = new ArrayList<>();
//
//                    List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
//
//                    for (CoreMap sentence : sentences) {
//                        // traversing the words in the current sentence
//                        // a CoreLabel is a CoreMap with additional token-specific methods
//                        for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
//                            // this is the text of the token
//                            String word = token.get(CoreAnnotations.TextAnnotation.class);
//                            // this is the POS tag of the token
//                            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
//                            // this is the NER label of the token
//                            //String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
//                            Log.d("Debugger",String.format("Print: word: [%s] pos: [%s] ne: [%s]", word, pos, "test") );
//                            words.add(word);
//                            POS_list.add(pos);
//
//
//                        }
//                    }

                    // END OF NLP AI CODE


                    //Speak all the parts of the speech back to the user
//                    String ft = "";
//                    for (int j = 0; j < words.size(); j++) {
//
//                        try {
//
//
//                            ft = ft + String.format("The part of speech of the word %s is a %s. ", words.get(j), map.get(POS_list.get(j)));
//                        } catch (Exception e)
//                        {
//                            ft = "Could not determine";
//                        }
//
//                    }
                    //textToSpeech.speak(ft, TextToSpeech.QUEUE_FLUSH, null);

                }
                break;
            }

        }
    }

    private class MyTTSInitListener implements OnInitListener {

        @Override
        public void onInit(int i) {
            int result = textToSpeech.setLanguage(Locale.US);
            //int result = textToSpeech.setLanguage(Locale.CHINA);
            int v1 = textToSpeech.isLanguageAvailable(Locale.CHINESE);
            int v2 = textToSpeech.isLanguageAvailable(Locale.US);
            int v3 = textToSpeech.isLanguageAvailable(Locale.SIMPLIFIED_CHINESE);
            int v4 = textToSpeech.isLanguageAvailable(Locale.CHINA);

            Log.i("TTT", "  " + v1 + v2 + v3 + v4);
            //textToSpeech.speak("你好", TextToSpeech.QUEUE_FLUSH, null);
            //textToSpeech.speak("hello", TextToSpeech.QUEUE_FLUSH, null);
            //textToSpeech.sp


            //textToSpeech
            // FOR NOW, DO NOTHING
        }
    };
}
